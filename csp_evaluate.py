import tweetnlp
from camel_tools.sentiment import SentimentAnalyzer

import pandas as pd

class cspEvaluate:
    def __init__(self, data):
        self.data = pd.read_csv(data)
        self.camelAnalyser = SentimentAnalyzer.pretrained()
        self.tweetnlp = tweetnlp.load_model('sentiment', multilingual=True)

    #tweet_nlp
    def tweetnlp_label(self):
        self.data['tweetnlp_label'] = self.data['Tweet_cleaned'].apply(lambda x:compute_sentiment(self.tweetnlp, x))
        return self.data

    def camel_label(self):
        self.data['camel_label']  = self.camelAnalyser.predict(self.data['Tweet_cleaned'].tolist())
        return self.data

    def match(self):
        self.data['match'] = self.data['camel_label'] == self.data['tweetnlp_label']
        return self.data['match'].count()/len(self.data)*100

def compute_sentiment(model, sentence):
    return model.sentiment(sentence)['label']

if __name__ == '__main__':
    eva = cspEvaluate("customer_perception/csp_data.csv")
    csp_camel_labels = eva.tweetnlp_label()
    print(csp_camel_labels[:10])