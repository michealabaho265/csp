import io
import chardet
import re

class Lexicon:
    def __init__(self, path_to_lex):
        """For a set of inputs, get the output the model produces.
            :param path_to_lex: path to the lexicon
            :param lexicon: Dictionary in which the lexicon contents are added
            :param mulitple_occurences: Dictionary with words in the lexiocn that appear more than once.
        """
        self.path = path_to_lex
        self.lexicon = {}
        self.multiple_occureces = {}

    #method to fetch lexicon from its raw file thatb uses the windows-1256 encoding
    def fetch_lex(self):
        try:
            with open(self.path, encoding='windows-1256') as p:
                for i,line in enumerate(p.readlines()):
                    if i > 71:
                        word, score = line.split()
                        if word not in self.lexicon:
                            self.lexicon[word] = [float(score)]
                        elif word in self.lexicon:
                            if float(score) in self.lexicon[word]:
                                pass
                            else:
                                self.lexicon[word].append(float(score))
                        else:
                            print("%s needs further inspection"%(word))
        except IOError as e:
            raise IOError("Something is wrng with either the encoding or the file")


    def get_lexicon_coverage(self, lexicon):
        return len(lexicon)

    def fetch_words_with_mltiple_occurrences(self, lexicon):
        """
           :param model: sentiment lexicon (dictionary)
            :return:
                a dictionary of lexical entries (words) which have multiple occurrences iwhtin the sentiment lexicon
         """
        multiple_occurence_dict = {}
        for i, j in lex.lexicon.items():
            if len(j) > 1:
                multiple_occurence_dict[i] = j
        return multiple_occurence_dict

if __name__ == "__main__":
    lex = Lexicon("AraSenti/AraSentiLexiconV1.0")
    lex.fetch_lex()

    # print(lex.lexicon)
    print("Lexicon coverage is %d"%(lex.get_lexicon_coverage(lex.lexicon)))
    print("Words with multiple occurrences\n", lex.fetch_words_with_mltiple_occurrences(lex.lexicon))
