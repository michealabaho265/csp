import pandas as pd
import re
import numpy as np
import getlexicon
import logging
import qalsadi.lemmatizer
from nltk.corpus import wordnet as wn
import time
from tabulate import tabulate
from multiprocessing import Pool
from numba import njit, types
from numba import np as numba_np
import matplotlib.pyplot as plt
from camel_tools.disambig.bert import BERTUnfactoredDisambiguator

logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)

class annotate:
    def __init__(self, path_to_data, path_to_lexicon):
        self.data = pd.read_csv(path_to_data)
        if 'Unnamed: 0' in self.data.columns:
            self.data.drop(columns=['Unnamed: 0'], axis=1, inplace=True)
        self.dataset_words = {}
        self.words_present = []
        self.words_absent = []
        self.lexicon = fetch_lexicon(path_to_lexicon)
        print(self.data.head(5))
        lex_dict_frame = pd.DataFrame.from_dict(self.lexicon.lexicon.items())
        lex_dict_frame.columns = ["Word", "List of sentiment scores"]
        print(tabulate(lex_dict_frame.head(5), headers='keys', tablefmt='psql'))


    def compute_dataset_coverage_stats(self):
        for sentence in self.data["Tweet_cleaned"]:
            sent_split = re.split(r' ', sentence)
            for word in sent_split:
                if word not in self.dataset_words:
                    self.dataset_words[word] = 1
                else:
                    self.dataset_words[word] += 1

                if word in self.lexicon.lexicon and word not in self.words_present:
                    self.words_present.append(word)
                elif word not in self.lexicon.lexicon and word not in self.words_absent:
                    self.words_absent.append(word)
                else:
                    pass
        lexicon_coverage = self.lexicon.get_lexicon_coverage(self.lexicon.lexicon)
        logging.info("The total number of dataset words is {}".format(len(self.dataset_words)))
        logging.info("The total number of dataset words present in the lexicon is {}".format(len(self.words_present)))
        logging.info("The total number of dataset words absent in the lexicon is {}".format(len(self.words_absent)))
        logging.info("Percentage of the dataset words in the lexicon {}".format(
            round(len(self.words_present)/len(self.dataset_words), 3)*100))
        logging.info("Percentage of the lexicon words in the dataset {}".format(
            round(len(self.words_present)/lexicon_coverage, 3)*100))

    def tag_sentiment(self, df):
        tagged_data = df
        tagged_data['sentiment'] = tagged_data['Tweet_cleaned'].apply(lambda row: compute_sentiment_score(row, self.lexicon.lexicon))
        return  tagged_data

    #Multiprocessing to speed up the computation of sentiment scores.
    def par(self, df, func, n_cores):
        tagged_data_split = np.array_split(df, n_cores)
        pool = Pool(n_cores)
        df = pd.concat(pool.map(func, tagged_data_split))
        pool.close()
        pool.join()
        return df

    def exec(self, sample=None, n_cores=6):
        sample = sample if sample != None else self.data.shape[0]
        df = self.par(self.data[:sample], self.tag_sentiment, n_cores=n_cores)
        return df

def fetch_lexicon(path_to_lexicon):
    lexicon = getlexicon.Lexicon(path_to_lexicon)
    lexicon.fetch_lex()
    return lexicon

def is_arabic_word(word):
    pattern = re.compile(r'[\u0600-\u06FF\u0750-\u077F\u08A0-\u08FF\uFB50-\uFDFF\uFE70-\uFEFF]+')
    match = pattern.match(word)
    return bool(match)

def compute_sentiment_score(sentence, lexicon):
    '''
    Function computes the sentiment score of a sentence
    If I search for the word “Playing” inside Sentiment Lexicon X (e.g. AraSenti), and I do not find it there?
        Steps of dealing with words missing in a Sentiment Lexicon:
        Search for its lemma inside a bigger dictionary such as WordNet.
        I found the lemma “play” for Playing inside WordNet (e.g. Arabic WordNet).
        If there are multiple lemma’s for Playing, take all of them.
        Go back and check Sentiment Lexicon X for the word play
        Use play’s sentiment score to represent Playing
    :param sentence: Tweet that is input into this function
    :param lexicon: AraSenti
    :return: a score for a sentence (the average sentiment score of all words inside the input sentence (tweet))
    '''
    ql = qalsadi.lemmatizer.Lemmatizer()
    disambiguator = BERTUnfactoredDisambiguator.pretrained()
    sent_split = sentence.split()

    def score_sentiment(sent_split):
        sentiment_score = 0
        for i,word in enumerate(sent_split):
            if word in lexicon:
                scores = lexicon[word]
                word_score = np.mean(scores)
                sentiment_score += word_score
            else:
                if is_arabic_word(word):
                    word_lemma = ql.lemmatize(word)
                    if word_lemma in lexicon:
                        word_lemma_score = np.mean(lexicon[word_lemma])
                        sentiment_score += word_lemma_score
                    else:
                        sentiment_score += 0
                    # word_synsets = disambiguator.disambiguate_word(sentence=sent_split, word_ndx=i)
                    # print("First synset", word_synsets)
                else:
                    sentiment_score += 0
        return sentiment_score

    sentiment_score = score_sentiment(sent_split)/len(sent_split)
    if sentiment_score > 0:
        return "positive"
    elif sentiment_score < 0:
        return "negative"
    else:
        return "neutral"

#perfor a binary search to search for a word in a lexicon
def binary_search(arr, target):
    low, high = 0, len(arr) - 1
    while low <= high:
        mid = (low + high) // 2
        if arr[mid] == target:
            return mid
        elif arr[mid] < target:
            low = mid + 1
        else:
            high = mid - 1
    return -1

if __name__ == '__main__':
    start_time = time.time()
    ann = annotate("customer_perception/csp_data.csv", "AraSenti/AraSentiLexiconV1.0")
    ann.compute_dataset_coverage_stats()
    annotated_data = ann.exec(sample=5, n_cores=1)
    print(ann.data.head())
    print(annotated_data.head())
    annotated_data.to_csv('customer_perception/annotated_csp.csv')
    per_sentiment = annotated_data.groupby(['sentiment'])['sentiment'].count()
    plt.figure(figsize=(8,8))
    per_sentiment.plot.bar()
    plt.xticks(rotation=0)
    plt.xlabel('Sentiment labels')
    plt.savefig('customer_perception/sentiment_label_distribution.png')
    logging.info("Duration of run is %fs"%(time.time() - start_time))